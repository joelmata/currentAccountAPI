package codechallenge.transaction.model;

/**
 * Rest response model class
 */
public class RestResponse {

    private String message; // the response message
    private String code;    // the service response html code

    public RestResponse(String message, String code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
