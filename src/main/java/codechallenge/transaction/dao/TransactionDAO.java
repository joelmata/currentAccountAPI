package codechallenge.transaction.dao;

import codechallenge.transaction.model.Transaction;

public interface TransactionDAO<T> {

    /**
     * Saves/updates an entity
     * @param t the entity to save/update
     * @return the saved/updated entity
     */
    Transaction save(T t);

    /**
     * Retrieves entities per their id
     * @param id the entity id
     * @return an iterable of entities with a given id
     */
    Iterable<T> findById(String id);
}
