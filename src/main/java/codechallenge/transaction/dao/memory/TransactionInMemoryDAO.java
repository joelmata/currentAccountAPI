package codechallenge.transaction.dao.memory;

import codechallenge.transaction.dao.TransactionDAO;
import codechallenge.transaction.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class TransactionInMemoryDAO implements TransactionDAO<Transaction> {

    private Logger logger = LoggerFactory.getLogger(TransactionInMemoryDAO.class);

    private ConcurrentHashMap<String, List<Transaction>> accountTransactionsHashMap = new ConcurrentHashMap<>();

    @Override
    public Transaction save(Transaction transaction) {
        List<Transaction> accountTransactionsList = accountTransactionsHashMap.get(transaction.getCurrentAccountId());
        if (accountTransactionsList == null) {
            accountTransactionsList = new ArrayList<>();
        }
        logger.info("Save new transaction for current_account_id: {}, amount: {}", transaction.getCurrentAccountId(), transaction.getAmount());
        accountTransactionsList.add(transaction);
        accountTransactionsHashMap.put(transaction.getCurrentAccountId(), accountTransactionsList);
        return transaction;
    }

    @Override
    public List<Transaction> findById(String id) {
        logger.info("Retrieve transactions by id: {}", id);
        return accountTransactionsHashMap.getOrDefault(id, new ArrayList<>());
    }
}
