package codechallenge.transaction.controllers;


import codechallenge.transaction.model.Transaction;
import codechallenge.transaction.service.TransactionServiceException;
import codechallenge.transaction.service.TransactionServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Validation controller
 */
@Controller
@RequestMapping("/transaction")
public class TransactionController {

    private Logger logger = LoggerFactory.getLogger(TransactionController.class);

    private TransactionServiceImpl transactionServiceImpl;

    @Autowired
    public TransactionController(TransactionServiceImpl transactionServiceImpl) {
        this.transactionServiceImpl = transactionServiceImpl;
    }

    /**
     * Adds a new current account transaction
     * @param currentAccountId the current account id
     * @param amount the transaction amount
     * @return the operation result
     */
    @PostMapping
    @ResponseBody
    public ResponseEntity<Object> transaction(@RequestParam("currentAccountId") String currentAccountId,
                           @RequestParam("amount") Double amount) {
        String message;
        HttpStatus httpStatus;
        try {
            transactionServiceImpl.add(currentAccountId, amount);

            message = String.format("Processing transaction for current_account_id: %s, amount: %s", currentAccountId, amount);
            httpStatus = HttpStatus.OK;
            logger.info(message);
        } catch (TransactionServiceException e) {
            message = e.getMessage();
            httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
            logger.error(message, e);
        } catch (Exception e) {
            message = String.format("Error while processing transaction for current_account_id: %s, amount: %s, error: %s", currentAccountId, amount, e.getMessage());
            httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
            logger.error(message, e);
        }
        return new ResponseEntity(message, httpStatus);
    }

    /**
     * Retrieves all transactions for a the given current account id
     * @param currentAccountId a current account id
     * @return a list with all transactions of a given current account id
     */
    @GetMapping
    @ResponseBody
    public ResponseEntity<Object> transaction(@RequestParam("currentAccountId") String currentAccountId) {
        String message;
        HttpStatus httpStatus;
        Iterable<Transaction> transactions = null;
        try {
            transactions = transactionServiceImpl.get(currentAccountId);

            message = String.format("Retrieving transactions for current_account_id: %s", currentAccountId);
            httpStatus = HttpStatus.OK;
            logger.info(message);
        } catch (TransactionServiceException e) {
            message = e.getMessage();
            httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
            logger.error(message, e);
        } catch (Exception e) {
            message = String.format("Error while retrieving transactions for current_account_id: %s, error: %s", currentAccountId, e.getMessage());
            httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
            logger.error(message, e);
        }
        return new ResponseEntity(transactions, httpStatus);
    }
}