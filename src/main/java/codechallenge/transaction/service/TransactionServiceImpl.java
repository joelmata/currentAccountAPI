package codechallenge.transaction.service;

import codechallenge.transaction.dao.TransactionDAO;
import codechallenge.transaction.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionDAO transactionDAO;

    @Autowired
    public TransactionServiceImpl(TransactionDAO transactionDAO) {
        this.transactionDAO = transactionDAO;
    }

    @Override
    public void add(String currentAccountId, Double amount) throws TransactionServiceException {
        try {
            transactionDAO.save(new Transaction(currentAccountId, amount));
        } catch (Exception e) {
            String message = String.format("Error while processing transaction for current_account_id: %s, amount: %s, error: %s", currentAccountId, amount, e.getMessage());
            throw new TransactionServiceException(message, e);
        }
    }

    @Override
    public Iterable get(String currentAccountId) throws TransactionServiceException {
        try {
            return transactionDAO.findById(currentAccountId);
        } catch (Exception e) {
            String message = String.format("Error while processing transaction for current_account_id: %s, error: %s", currentAccountId, e.getMessage());
            throw new TransactionServiceException(message, e);
        }
    }
}