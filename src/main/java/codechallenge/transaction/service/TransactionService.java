package codechallenge.transaction.service;

public interface TransactionService {
    /**
     * Adds a new transaction
     * @param currentAccountId the current account id
     * @param amount the amount of the transaction
     * @throws TransactionServiceException
     */
    void add(String currentAccountId, Double amount) throws TransactionServiceException;

    /**
     * Retrieves all transactions given a current account id
     * @param currentAccountId the current account id
     * @throws TransactionServiceException
     * @return a iterator for a current account transactions
     */
    Iterable get(String currentAccountId) throws TransactionServiceException;
}