package codechallenge.transaction.service;

public class TransactionServiceException extends Exception {

    public TransactionServiceException(String message, Exception e) {
        super(message, e);
    }
}
