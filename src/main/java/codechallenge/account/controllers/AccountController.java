package codechallenge.account.controllers;


import codechallenge.account.model.CurrentAccount;
import codechallenge.account.model.RestResponse;
import codechallenge.account.service.AccountService;
import codechallenge.account.transaction.service.TransactionRestServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Account controller
 */
@Controller
public class AccountController {

    private Logger logger = LoggerFactory.getLogger(AccountController.class);

    private AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * Adds a new current account
     * @param customerId the customer id
     * @param initialCredit the account initial credit
     * @return the operation result
     */
    @PostMapping("/account")
    @ResponseBody
    public ResponseEntity account(@RequestParam("customerId") String customerId,
                                  @RequestParam("initialCredit") Double initialCredit) {
        String message;
        HttpStatus httpStatus;
        try {
            CurrentAccount currentAccount = accountService.add(customerId, initialCredit);

            message = String.format("Open new current account number: %s, for customer_id: %s, initial_credit: %s", currentAccount.getId(), customerId, initialCredit);
            httpStatus = HttpStatus.OK;
            logger.info(message);
        } catch (TransactionRestServiceException e) {
            message = e.getMessage();
            httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
            logger.error(message, e);
        } catch (Exception e) {
            message = String.format("Error while opening new current account for customer_id: %s, initial_credit: %s, error: %s", customerId, initialCredit, e.getMessage());
            httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
            logger.error(message, e);
        }
        return new ResponseEntity(new RestResponse(message, httpStatus.toString()), httpStatus);
    }
}