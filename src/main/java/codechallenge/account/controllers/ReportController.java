package codechallenge.account.controllers;


import codechallenge.account.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Report controller
 */
@Controller
public class ReportController {

    private Logger logger = LoggerFactory.getLogger(ReportController.class);

    private AccountService accountService;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public ReportController(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * Retrieves a customer current accounts report
     * @param customerId the customer id
     * @return a json depicting a customer current accounts report
     */
    @GetMapping("/report")
    @ResponseBody
    public ResponseEntity report(@RequestParam("customerId") String customerId) {
        String message;
        HttpStatus httpStatus;
        try {
            message = objectMapper.writeValueAsString(accountService.report(customerId));
            httpStatus = HttpStatus.OK;
            logger.info(message);
        } catch (Exception e) {
            message = String.format("Error while generating the current accounts report for customer_id: %s, error: %s", customerId, e.getMessage());
            httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
            logger.error(message);
        }
        return new ResponseEntity(message, httpStatus);
    }
}