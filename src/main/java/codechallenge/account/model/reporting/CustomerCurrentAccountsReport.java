package codechallenge.account.model.reporting;


import codechallenge.account.model.Transaction;

import java.util.List;
import java.util.UUID;

public class CustomerCurrentAccountsReport {
    private UUID currentAccountId;
    private List<Transaction> transactions;
    private Double balance;

    public void setCurrentAccountId(UUID currentAccountId) {
        this.currentAccountId = currentAccountId;
    }

    public UUID getCurrentAccountId() {
        return currentAccountId;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getBalance() {
        return balance;
    }
}
