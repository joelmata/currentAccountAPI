package codechallenge.account.model.reporting;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Customer report model class
 */
public class CustomerReport {

    private String customerId;

    @JsonProperty("currentAccounts")
    private List<CustomerCurrentAccountsReport> customerCurrentAccountsReportList = new ArrayList<>();

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public List<CustomerCurrentAccountsReport> getCustomerCurrentAccountsReportList() {
        return customerCurrentAccountsReportList;
    }

    public void addCurrentAccountReport(CustomerCurrentAccountsReport customerCurrentAccountsReport) {
        this.customerCurrentAccountsReportList.add(customerCurrentAccountsReport);
    }
}
