package codechallenge.account.model;

/**
 * Enumerator for the various status a current account can have
 */
public enum CurrentAcountStatus {
    SETUP,  // the account is setup
            // new current accounts in this stage are waiting for the initial transaction to succeed/be confirmed
    OPEN    // the account is open and ready for usage
}
