package codechallenge.account.model;

public enum Status {
    SETUP,  // the account is setup
            // new current accounts in this stage are waiting for the initial transaction to succeed/be confirmed
    OPEN    // the account is open and ready for usage
}
