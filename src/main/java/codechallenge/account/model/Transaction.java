package codechallenge.account.model;

/**
 * Models an current account transaction
 */
public class Transaction {

    private final String currentAccountId;
    private final Double amount;

    public Transaction(String currentAccountId, Double amount) {
        this.currentAccountId = currentAccountId;
        this.amount = amount;
    }

    public String getCurrentAccountId() {
        return currentAccountId;
    }

    public Double getAmount() {
        return amount;
    }
}
