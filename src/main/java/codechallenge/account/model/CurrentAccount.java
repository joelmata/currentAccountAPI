package codechallenge.account.model;

import java.time.Instant;
import java.util.UUID;

/**
 * Current account model class
 */
public class CurrentAccount {

    private UUID id;    // TODO replace with a Long before going into production
    private String customerId;
    private Double initialCredit;
    private CurrentAcountStatus currentAcountStatus;
    private Instant updated;

    public CurrentAccount(String customerId, Double initialCredit, CurrentAcountStatus currentAcountStatus) {
        this.customerId = customerId;
        this.initialCredit = initialCredit;
        this.currentAcountStatus = currentAcountStatus;
        this.updated = Instant.now();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Double getInitialCredit() {
        return initialCredit;
    }

    public void setInitialCredit(Double initialCredit) {
        this.initialCredit = initialCredit;
    }

    public CurrentAcountStatus getCurrentAcountStatus() {
        return currentAcountStatus;
    }

    public void setCurrentAcountStatus(CurrentAcountStatus currentAcountStatus) {
        this.currentAcountStatus = currentAcountStatus;
    }

    public Instant getUpdated() {
        return updated;
    }

    public void setUpdated(Instant updated) {
        this.updated = updated;
    }
}
