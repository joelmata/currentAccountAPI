package codechallenge.account.dao.memory;

import codechallenge.account.dao.AccountDAO;
import codechallenge.account.model.CurrentAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class AccountInMemoryAccountDAO implements AccountDAO<CurrentAccount> {

    private Logger logger = LoggerFactory.getLogger(AccountInMemoryAccountDAO.class);

    private ConcurrentHashMap<String, List<CurrentAccount>> customerCurrentAccountHashMap = new ConcurrentHashMap<>();

    @Override
    public CurrentAccount save(CurrentAccount currentAccount) {
        List<CurrentAccount> customerCurrentAccountsList = customerCurrentAccountHashMap.get(currentAccount.getCustomerId());
        if (customerCurrentAccountsList == null) {
            customerCurrentAccountsList = new ArrayList<>();
        }

//        this is a new current account or update?
        if(currentAccount.getId() == null) {
            currentAccount.setId(UUID.randomUUID());
            customerCurrentAccountsList.add(currentAccount);
            customerCurrentAccountHashMap.put(currentAccount.getCustomerId(), customerCurrentAccountsList);
            logger.info("Save current account for customer_id: {}, initial_credit: {}, status: {}", currentAccount.getCustomerId(), currentAccount.getInitialCredit(), currentAccount.getCurrentAcountStatus().name());
        } else {
            delete(currentAccount);
            customerCurrentAccountsList = customerCurrentAccountHashMap.get(currentAccount.getCustomerId());
            customerCurrentAccountsList.add(currentAccount);
            customerCurrentAccountHashMap.put(currentAccount.getCustomerId(), customerCurrentAccountsList);
            logger.info("Updated current account for customer_id: {}, initial_credit: {}, status: {}", currentAccount.getCustomerId(), currentAccount.getInitialCredit(), currentAccount.getCurrentAcountStatus().name());
        }
        return currentAccount;
    }

    @Override
    public void delete(CurrentAccount currentAccount) {
        logger.info("Delete current account: {} for customer_id: {}, initial_credit: {}", currentAccount.getId(), currentAccount.getCustomerId(), currentAccount.getInitialCredit());
        customerCurrentAccountHashMap.get(currentAccount.getCustomerId()).removeIf(x -> x.getId().toString().equals(currentAccount.getId().toString()));
    }

    @Override
    public Iterable<CurrentAccount> findById(String id) {
        logger.info("Retrieve current accounts by id: {}", id);
        return customerCurrentAccountHashMap.getOrDefault(id, new ArrayList<>());
    }
}
