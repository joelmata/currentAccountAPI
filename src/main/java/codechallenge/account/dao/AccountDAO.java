package codechallenge.account.dao;

import codechallenge.account.model.CurrentAccount;

public interface AccountDAO<T> {

    /**
     * Saves/updates an entity
     * @param t the entity to save/update
     * @return the saved/updated entity
     */
    CurrentAccount save(T t);

    /**
     * Deletes an entity
     * @param t the entity
     */
    void delete(T t);

    /**
     * Retrieves entities per their id
     * @param id the entity id
     * @return an iterable of entities with a given id
     */
    Iterable<T> findById(String id);
}
