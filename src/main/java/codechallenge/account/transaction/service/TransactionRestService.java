package codechallenge.account.transaction.service;

import codechallenge.account.model.Transaction;
import codechallenge.transaction.service.TransactionServiceException;

import java.util.List;
import java.util.UUID;

public interface TransactionRestService {

    /**
     * Sends a new transaction to the transaction service
     * @param customerID the customer id
     * @param amount the transaction amount
     */
    void add(String customerID, Double amount) throws TransactionRestServiceException;

    /**
     * Retrieves all transactions given a current account id
     * @param currentAccountId the current account id
     * @throws TransactionServiceException
     * @return a iterator for a current account transactions
     */
    List<Transaction> get(UUID currentAccountId) throws TransactionRestServiceException;
}
