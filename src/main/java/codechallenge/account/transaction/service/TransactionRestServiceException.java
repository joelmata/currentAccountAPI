package codechallenge.account.transaction.service;

public class TransactionRestServiceException extends Exception {

    public TransactionRestServiceException(String message, Exception e) {
        super(message, e);
    }
}
