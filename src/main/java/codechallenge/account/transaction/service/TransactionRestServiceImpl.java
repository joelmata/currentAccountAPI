package codechallenge.account.transaction.service;

import codechallenge.account.model.Transaction;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

@Service
public class TransactionRestServiceImpl implements codechallenge.account.transaction.service.TransactionRestService {

    private static final String CURRENT_ACCOUNT_ID = "currentAccountId";
    private static final String AMOUNT = "amount";
    private static final String TRANSACTION_API_BASE_URL = "/transaction";

    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${transaction.api.host}")
    private String transactionAPIHost;

    @Override
    public void add(String currentAccountId, Double amount) throws TransactionRestServiceException {
        final String uri = transactionAPIHost + TRANSACTION_API_BASE_URL;

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add(CURRENT_ACCOUNT_ID, currentAccountId);
        map.add(AMOUNT, amount);
        try {
            restTemplate.postForObject(uri, map, String.class);
        } catch (Exception e) {
            String message = String.format("Error adding initial transaction for new current account for current_account_id: %s, amount: %s, error: %s", currentAccountId, amount, e.getMessage());
            throw new TransactionRestServiceException(message, e);
        }
    }

    @Override
    public List<Transaction> get(UUID id) throws TransactionRestServiceException {
        final String uri = transactionAPIHost + TRANSACTION_API_BASE_URL + "?currentAccountId={currentAccountId}";
        ResponseEntity<ArrayList> restResponse;
        List<Transaction> transactionList = new ArrayList<>();
        try {
            restResponse = restTemplate.getForEntity(uri, ArrayList.class, id.toString());
            if (restResponse.getBody() != null) {
                restResponse.getBody().forEach(transaction -> transactionList.add(new Transaction((String)(((LinkedHashMap)transaction).get(CURRENT_ACCOUNT_ID)),
                            (Double)(((LinkedHashMap)transaction).get(AMOUNT)))));
            }
        } catch (Exception e) {
            String message = String.format("Error getting transactions for current_account_id: %s, error: %s", id, e.getMessage());
            throw new TransactionRestServiceException(message, e);
        }
        return transactionList;
    }
}
