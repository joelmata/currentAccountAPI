package codechallenge.account.service;

public class AccountServiceException extends Exception {

    public AccountServiceException(String message, Exception e) {
        super(message, e);
    }
}
