package codechallenge.account.service;

import codechallenge.account.dao.AccountDAO;
import codechallenge.account.model.CurrentAccount;
import codechallenge.account.model.CurrentAcountStatus;
import codechallenge.account.model.Transaction;
import codechallenge.account.model.reporting.CustomerCurrentAccountsReport;
import codechallenge.account.model.reporting.CustomerReport;
import codechallenge.account.transaction.service.TransactionRestService;
import codechallenge.account.transaction.service.TransactionRestServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Concrete implementation of a "to" file system storage service
 */
@Component
public class AccountServiceImpl implements AccountService {

    private AccountDAO accountDao;
    private TransactionRestService transactionRestService;

    @Autowired
    public AccountServiceImpl(AccountDAO accountDao,
                              TransactionRestService transactionRestService) {
        this.accountDao = accountDao;
        this.transactionRestService = transactionRestService;
    }

    @Override
    @Retryable(value={TransactionRestServiceException.class}, maxAttempts = 5, backoff = @Backoff(delay = 250))
    public CurrentAccount add(String customerId, Double initialCredit) throws TransactionRestServiceException, AccountServiceException {
        CurrentAccount currentAccount = new CurrentAccount(customerId,
                initialCredit,
                CurrentAcountStatus.SETUP);
        currentAccount = accountDao.save(currentAccount);
        if (initialCredit > 0) {
            try {
//                sends a new transaction to the transaction service for the amount of the initial credit
                transactionRestService.add(currentAccount.getId().toString(), initialCredit);
            } catch (Exception e) {
                accountDao.delete(currentAccount);
                String message = String.format("Opening a new current account for customer_id: %s with account_id: %s failed, initial transaction was not processed. Error: %s", customerId, currentAccount.getId(), e.getMessage());
                throw new AccountServiceException(message, e);
            }
        }
        currentAccount.setCurrentAcountStatus(CurrentAcountStatus.OPEN);
        return accountDao.save(currentAccount);
    }

    @Override
    public CustomerReport report(String customerId) throws TransactionRestServiceException {
        CustomerReport customerReport = new CustomerReport();
        customerReport.setCustomerId(customerId);

        Iterable<CurrentAccount> customerIdAccountsIterable = accountDao.findById(customerId);
        for (CurrentAccount currentAccount : customerIdAccountsIterable) {
            CustomerCurrentAccountsReport customerCurrentAccountsReport = new CustomerCurrentAccountsReport();
            customerCurrentAccountsReport.setCurrentAccountId(currentAccount.getId());

            List<Transaction> transactionList = transactionRestService.get(currentAccount.getId());
            customerCurrentAccountsReport.setTransactions(transactionList);

//            determine the current account total
            AtomicReference<Double> balance = new AtomicReference<>(0D);
            transactionList.forEach(localTransaction -> balance.set(balance.get() + localTransaction.getAmount()));
            customerCurrentAccountsReport.setBalance(balance.get());

            customerReport.addCurrentAccountReport(customerCurrentAccountsReport);
        }
        return customerReport;
    }
}