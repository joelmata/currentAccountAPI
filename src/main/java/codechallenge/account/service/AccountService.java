package codechallenge.account.service;

import codechallenge.account.model.CurrentAccount;
import codechallenge.account.model.reporting.CustomerReport;
import codechallenge.account.transaction.service.TransactionRestServiceException;
import org.springframework.stereotype.Service;

@Service
public interface AccountService {

    /**
     * Adds a new current account to a customer
     * @param customerId the customer Id
     * @param initialCredit the customers new account initial credit
     * @return the added current account
     * @throws Exception
     */
    CurrentAccount add(String customerId, Double initialCredit) throws TransactionRestServiceException, AccountServiceException;

    /**
     * Generates a customer current accounts report
     * @param customerId the customer Id
     * @return the customer current accounts report
     * @throws Exception
     */
    CustomerReport report(String customerId) throws TransactionRestServiceException;
}
