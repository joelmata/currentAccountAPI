package codechallenge.transaction.controllers;


import codechallenge.transaction.service.TransactionServiceImpl;
import codechallenge.transaction.service.TransactionServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@AutoConfigureMockMvc
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TransactionControllerTest {

    @MockBean
    private TransactionServiceImpl transactionServiceImpl;

    private TransactionController transactionController;

    private MockMvc mvc;

    private MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

    @Before
    public void setup() {
        map.add("currentAccountId", "123456789");
        map.add("amount", "100");
    }

    @Test
    public void given_new_transaction_when_call_with_post_then_ok() throws Exception {
        transactionController = Mockito.mock(TransactionController.class);
        mvc = MockMvcBuilders.standaloneSetup(transactionController).build();

//        given
        MockHttpServletResponse response = mvc.perform(
                org.springframework.test.web.servlet.request.MockMvcRequestBuilders
                        .post("/transaction")
                        .params(map))
                .andReturn().getResponse();

//        then
        Assert.assertEquals(response.getStatus(), HttpStatus.OK.value());
    }

    @Test
    public void given_new_transaction_when_call_with_put_then_method_not_allowed() throws Exception {
        transactionController = Mockito.mock(TransactionController.class);
        mvc = MockMvcBuilders.standaloneSetup(transactionController).build();

//        given
        MockHttpServletResponse response = mvc.perform(
                org.springframework.test.web.servlet.request.MockMvcRequestBuilders
                        .put("/transaction")
                        .params(map))
                .andReturn().getResponse();

//        then
        Assert.assertEquals(response.getStatus(), HttpStatus.METHOD_NOT_ALLOWED.value());
    }

    @Test
    public void given_new_transaction_when_call_without_customerid_then_not_ok() throws Exception {
        transactionController = Mockito.mock(TransactionController.class);
        mvc = MockMvcBuilders.standaloneSetup(transactionController).build();

//        given
        map.remove("currentAccountId");
        MockHttpServletResponse response = mvc.perform(
                org.springframework.test.web.servlet.request.MockMvcRequestBuilders
                        .post("/transaction")
                        .params(map))
                .andReturn().getResponse();

//        then
        Assert.assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void given_new_transaction_when_call_without_amount_then_not_ok() throws Exception {
        transactionController = Mockito.mock(TransactionController.class);
        mvc = MockMvcBuilders.standaloneSetup(transactionController).build();

//        given
        map.remove("amount");
        MockHttpServletResponse response = mvc.perform(
                org.springframework.test.web.servlet.request.MockMvcRequestBuilders
                        .post("/transaction")
                        .params(map))
                .andReturn().getResponse();

//        then
        Assert.assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void given_new_transaction_when_add_throws_exception_then_not_ok() throws Exception {
        transactionController = new TransactionController(transactionServiceImpl);
        mvc = MockMvcBuilders.standaloneSetup(transactionController).build();

//        given
        Mockito.doThrow(TransactionServiceException.class).when(transactionServiceImpl).add(Mockito.anyString(), Mockito.anyDouble());

        MockHttpServletResponse response = mvc.perform(
                org.springframework.test.web.servlet.request.MockMvcRequestBuilders
                        .post("/transaction")
                        .params(map))
                .andReturn().getResponse();

//        then
        Assert.assertEquals(response.getStatus(), HttpStatus.UNPROCESSABLE_ENTITY.value());
    }
}