package codechallenge.account.controllers;


import codechallenge.account.service.AccountService;
import codechallenge.account.transaction.service.TransactionRestServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@AutoConfigureMockMvc
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AccountControllerTest {

    @MockBean
    private AccountService accountService;

    private AccountController accountController;

    private MockMvc mvc;

    private MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

    @Before
    public void setup() {
        map.add("customerId", "123456789");
        map.add("initialCredit", "100");
    }

    @Test
    public void given_call_with_post_then_ok() throws Exception {
        accountController = Mockito.mock(AccountController.class);
        mvc = MockMvcBuilders.standaloneSetup(accountController).build();

//        given
        MockHttpServletResponse response = mvc.perform(
                org.springframework.test.web.servlet.request.MockMvcRequestBuilders
                        .post("/account")
                        .params(map))
                .andReturn().getResponse();

//        then
        Assert.assertEquals(response.getStatus(), HttpStatus.OK.value());
    }

    @Test
    public void given_call_with_get_then_method_not_allowed() throws Exception {
        accountController = Mockito.mock(AccountController.class);
        mvc = MockMvcBuilders.standaloneSetup(accountController).build();

//        given
        MockHttpServletResponse response = mvc.perform(
                org.springframework.test.web.servlet.request.MockMvcRequestBuilders
                        .get("/account")
                        .params(map))
                .andReturn().getResponse();

//        then
        Assert.assertEquals(response.getStatus(), HttpStatus.METHOD_NOT_ALLOWED.value());
    }

    @Test
    public void given_call_with_put_then_method_not_allowed() throws Exception {
        accountController = Mockito.mock(AccountController.class);
        mvc = MockMvcBuilders.standaloneSetup(accountController).build();

//        given
        MockHttpServletResponse response = mvc.perform(
                org.springframework.test.web.servlet.request.MockMvcRequestBuilders
                        .put("/account")
                        .params(map))
                .andReturn().getResponse();

//        then
        Assert.assertEquals(response.getStatus(), HttpStatus.METHOD_NOT_ALLOWED.value());
    }

    @Test
    public void given_call_without_customerid_then_not_ok() throws Exception {
        accountController = Mockito.mock(AccountController.class);
        mvc = MockMvcBuilders.standaloneSetup(accountController).build();

//        given
        map.remove("customerId");
        MockHttpServletResponse response = mvc.perform(
                org.springframework.test.web.servlet.request.MockMvcRequestBuilders
                        .post("/account")
                        .params(map))
                .andReturn().getResponse();

//        then
        Assert.assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void given_call_without_initial_credit_then_not_ok() throws Exception {
        accountController = Mockito.mock(AccountController.class);
        mvc = MockMvcBuilders.standaloneSetup(accountController).build();

//        given
        map.remove("initialCredit");
        MockHttpServletResponse response = mvc.perform(
                org.springframework.test.web.servlet.request.MockMvcRequestBuilders
                        .post("/account")
                        .params(map))
                .andReturn().getResponse();

//        then
        Assert.assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void given_new_account_when_add_throws_exception_then_not_ok() throws TransactionRestServiceException, Exception {
        accountController = new AccountController(accountService);
        mvc = MockMvcBuilders.standaloneSetup(accountController).build();

//        given
        Mockito.doThrow(TransactionRestServiceException.class).when(accountService).add(Mockito.anyString(), Mockito.anyDouble());

//        when
        MockHttpServletResponse response = mvc.perform(
                org.springframework.test.web.servlet.request.MockMvcRequestBuilders
                        .post("/account")
                        .params(map))
                .andReturn().getResponse();

//        then
        Assert.assertEquals(response.getStatus(), HttpStatus.UNPROCESSABLE_ENTITY.value());
    }
}