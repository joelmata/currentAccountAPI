package codechallenge.account.service;

import codechallenge.account.dao.AccountDAO;
import codechallenge.account.model.CurrentAccount;
import codechallenge.account.model.CurrentAcountStatus;
import codechallenge.account.transaction.service.TransactionRestService;
import codechallenge.account.transaction.service.TransactionRestServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountServiceImplRetriableTest {

    @Autowired
    private AccountService accountService;

    private static AccountDAO accountDAO;
    private static TransactionRestService transactionRestService;

    @Test
    public void given_initial_transaction_fails_then_retry() throws TransactionRestServiceException {
        try {
            this.accountService.add("123456789", 100.0);
        } catch (Exception e) {
            Mockito.verify(transactionRestService, Mockito.times(5)).add(Mockito.any(), Mockito.any());
        }
    }

    @Configuration
    @EnableRetry
    public static class SpringConfig {

        @Bean
        public AccountService accountService() throws TransactionRestServiceException {
            accountDAO = Mockito.mock(AccountDAO.class);
            CurrentAccount currentAccount =  new CurrentAccount("123456789", 100.0, CurrentAcountStatus.SETUP);
            currentAccount.setId(UUID.randomUUID());
            Mockito.when(accountDAO.save(Mockito.any())).thenReturn(currentAccount);

            transactionRestService = Mockito.mock(TransactionRestService.class);
            Mockito.doThrow(TransactionRestServiceException.class)
                    .when(transactionRestService).add(currentAccount.getCustomerId(),currentAccount.getInitialCredit());

            AccountServiceImpl accountService = new AccountServiceImpl(accountDAO, transactionRestService);
            AccountServiceImpl accountServiceMock = Mockito.spy(accountService);
            return accountServiceMock;
        }
    }
}
