package codechallenge.account.service;

import codechallenge.account.dao.AccountDAO;
import codechallenge.account.model.CurrentAccount;
import codechallenge.account.model.CurrentAcountStatus;
import codechallenge.account.transaction.service.TransactionRestService;
import codechallenge.account.transaction.service.TransactionRestServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.UUID;

public class AccountServiceImplTest {

    private AccountDAO accountDAO;
    private TransactionRestService transactionRestService;
    private AccountServiceImpl accountService;

    @Before
    public void before(){
        transactionRestService = Mockito.mock(TransactionRestService.class);
        accountDAO = Mockito.mock(AccountDAO.class);
        accountService = new AccountServiceImpl(accountDAO, transactionRestService);
    }

    @Test
    public void given_initial_credit_is_zero_when_create_account_then_no_transaction_is_done() throws Exception {
//        given initial credit > 0
        Double initialCredit = 0D;
        CurrentAccount currentAccount =  new CurrentAccount("123456789", initialCredit, CurrentAcountStatus.SETUP);
        currentAccount.setId(UUID.randomUUID());
        Mockito.when(accountDAO.save(Mockito.any())).thenReturn(currentAccount);
//        when opening new current account
        accountService.add("123456789", initialCredit);
//        then a transaction is store
        Mockito.verify(transactionRestService, Mockito.times(0)).add(Mockito.anyString(), Mockito.anyDouble());
    }

    @Test
    public void given_initial_credit_higher_then_zero_when_create_account_then_new_transaction() throws Exception, TransactionRestServiceException {
//        given initial credit > 0
        Double initialCredit = 100D;
        CurrentAccount currentAccount =  new CurrentAccount("123456789", initialCredit, CurrentAcountStatus.SETUP);
        currentAccount.setId(UUID.randomUUID());
        Mockito.when(accountDAO.save(Mockito.any())).thenReturn(currentAccount);
//        when opening new current account
        accountService.add("123456789", initialCredit);
//        then a transaction is store
        Mockito.verify(transactionRestService, Mockito.times(1)).add(Mockito.anyString(), Mockito.anyDouble());
    }

    @Test
    public void given_initial_credit_higher_then_zero_when_create_account_then_new_current_account_is_stored() throws Exception, TransactionRestServiceException {
//        given initial credit > 0
        Double initialCredit = 100D;
        CurrentAccount currentAccount =  new CurrentAccount("123456789", initialCredit, CurrentAcountStatus.SETUP);
        currentAccount.setId(UUID.randomUUID());
        Mockito.when(accountDAO.save(Mockito.any())).thenReturn(currentAccount);
//        when opening new current account
        accountService.add("123456789", initialCredit);
//        then a current account is initialized
        Mockito.verify(accountDAO, Mockito.times(2)).save(Mockito.any());
//        ArgumentCaptor<CurrentAccount> currentAccountArgumentCaptor = ArgumentCaptor.forClass(CurrentAccount.class);
//        Mockito.verify(accountDAO, Mockito.times(2)).save(currentAccountArgumentCaptor.capture());
//        Assert.assertEquals(currentAccountArgumentCaptor.getValue().getStatus(), Status.SETUP);
    }

    @Test
    public void given_initial_credit_higher_then_zero_when_create_account_then_new_current_account_status_updated() throws Exception, TransactionRestServiceException {
//        given initial credit > 0
        Double initialCredit = 100D;
        CurrentAccount currentAccount =  new CurrentAccount("123456789", initialCredit, CurrentAcountStatus.SETUP);
        currentAccount.setId(UUID.randomUUID());
        Mockito.when(accountDAO.save(Mockito.any())).thenReturn(currentAccount);
//        when opening new current account
        accountService.add("123456789", initialCredit);
//        then a current account is open
        Mockito.verify(accountDAO, Mockito.times(2)).save(Mockito.any());
//        ArgumentCaptor<CurrentAccount> currentAccountArgumentCaptor = ArgumentCaptor.forClass(CurrentAccount.class);
//        Mockito.verify(accountDAO, Mockito.times(1)).save(currentAccountArgumentCaptor);
//        Assert.assertEquals(currentAccountArgumentCaptor.getValue().getStatus(), Status.OPEN);
    }

    @Test
    public void given_initial_credit_higher_then_zero_when_create_account_then_new_current_account_deleted() throws Exception {
//        given initial credit > 0
        Double initialCredit = 100D;
//        when opening new current account with initial credit > 0 and the initial transaction fails
        Mockito.doThrow(TransactionRestServiceException.class).when(transactionRestService).add(Mockito.any(), Mockito.any());
        accountService = new AccountServiceImpl(accountDAO, transactionRestService);
        try {
            accountService.add("123456789", initialCredit);
        } catch (Exception e) {
        }
//        then the current account is deleted
        Mockito.verify(accountDAO, Mockito.times(1)).delete(Mockito.any());
    }
}