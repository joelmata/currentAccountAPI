# Current Account API

__Design patterns__
Saga: 
1. Account API creates an account in Setup status, 
2. Connects to Transaction API for setup the initial transaction
3. Retries in case of error
4. If everything is ok the Account is set with status Open
5. Otherwise the Account is removed

__Circuit Breaker__
1. Account API creates an account in Setup status, 
2. Connects to Transaction API for setup the initial transaction
3. Retries N times in case of error

MVC: Along most of the project APIs

## Usage

### Run application
```
mvn spring-boot:run
```

### Testing

#### Unit testing
```
mvn test
```

#### Integration tests with Postman/newman

__Installing newman__
```
npm install -g newman
```
Run application (in terminal 1)
```
mvn spring-boot:run
```
Run newman (in terminal 2)
```
newman run CurrentAccountAPI.postman_collection.json 
```

## API definition
Check Postman file `AccountAPI.postman_collection.json` attached to this repo for API definition and usage examples.